echo "Make sure that enough memory is available for picrust2"

source /root/miniconda/bin/activate && conda activate picrust2

cwltool --debug --leave-tmpdir  --no-container --cachedir CACHEDIR /unlock/infrastructure/cwl/workflows/workflow_ngtax_picrust2.cwl AmpliconAnalysis_NGTAX_Silva138.1_100.yaml
