#!/bin/bash
#==========================================================================================
#title          :Metagenomics binning CWL workflow dependencies
#description    :Metagenomics binning  CWL workflow setup all dependencies
#author         :Bart Nijsse & Jasper Koehorst
#date           :2022
#version        :0.0.1
#==========================================================================================

# This script is expected to be run from the UNLOCK cwl docker container:
# https://git.wur.nl/unlock/docker/-/tree/master/cwl
# docker run -v <your dep dir>:/unlock docker-registry.wur.nl/unlock/docker:cwl /bin/bash -c "/cwl/setup_dependencies.sh"
# detailed instructions here: https://m-unlock.gitlab.io/docs/setup/setup.html

echo -e '\033[1;30m\033[44m                   Workflow Metagenomics Binning                      \033[0m';

# Set and create folders
conda_location=/unlock/infrastructure/conda
mkdir -p $conda_location
python_venv_location=/unlock/infrastructure/python_venv
mkdir -p python_venv_location
R_location=/unlock/infrastructure/R/library
mkdir -p $R_location

# Tools
echo -e '\n\033[1;33m Downloading tool binaries.. \033[0m'

tool_locations=( \
"MetaBAT/metabat_v2.12.1" \
"misc/"
)
for tool in ${tool_locations[@]}; do\
  echo -e '\033[1;33m  '$tool '\033[0m'
  lftp --user anonymous --password anonymous -e "mirror -c -e --parallel=8 --verbose binaries/$tool /unlock/infrastructure/binaries/$tool ;quit;" https://unlock-icat.irods.surfsara.nl:/infrastructure/binaries
done

# scripts
echo -e '\n\033[1;33m Setting up scripts.. \033[0m'
lftp --user anonymous --password anonymous -e "mirror -c -e --parallel=8 --verbose scripts/ /unlock/infrastructure/scripts/ ;quit;" https://unlock-icat.irods.surfsara.nl:/infrastructure/

# Tool permissions, because webdav does not retain permissions. 
echo -e '\n\033[1;33m Setting up executing rights.. \033[0m'

chmod +x /unlock/infrastructure/binaries/MetaBAT/metabat_v2.12.1/*
chmod +x /unlock/infrastructure/binaries/misc/raw_n50

################################################################
# Conda environments
################################################################
echo -e '\n\033[1;33m Setting up CONDA environments.. \033[0m'

# Update conda
source /root/miniconda/bin/activate
# !! THIS UPDATE RUN CURRENTLY BREAKS MAMBA !!
#conda update -y -n base -c defaults conda

conda_environments=( \
"busco/busco_v5.4.3" \
"maxbin2/maxbin_v2.2.7" \
"gtdbtk/gtdbtk_v1.7.0" \
"quast/quast_v5.2.0" \
)

for conda_env in ${conda_environments[@]}; do\
  tool=`echo $conda_env | awk -F"/" '{print $1}'`
  version=`echo $conda_env | awk -F"_" '{print $NF}' | sed 's/v//g'`

  if [[ ! -d "$conda_location/$conda_env" ]]
  then
    echo -e '\033[1;33m '$conda_env '\033[0m'
    mkdir -p $conda_location/$tool
    mamba create --yes --prefix $conda_location/$conda_env -c bioconda -c conda-forge $tool=$version
  else
    echo -e '\033[1;33m  conda environment ' $conda_env ' already present. Skipping.. \033[0m'
  fi
done

#GTGB-Tk location
echo "export GTDBTK_DATA_PATH=/unlock/references/databases/GTDBTK/release202/" > $conda_location/gtdbtk/gtdbtk_v1.7.0/etc/conda/activate.d/gtdbtk.sh

# CheckM
if [[ ! -d "$conda_location/checkm-genome/checkm-genome_v1.2.0" ]]
then
    echo -e '\033[1;33m  checkm-genome/checkm-genome_v1.2.0'
    mkdir -p $conda_location/checkm-genome/
    mamba create --yes --prefix $conda_location/checkm-genome/checkm-genome_v1.2.0 -c conda-forge -c bioconda checkm-genome=1.2.0
    conda activate $conda_location/checkm-genome/checkm-genome_v1.2.0
    checkm data setRoot /unlock/references/databases/CheckM/
    conda deactivate
  else
    echo -e '\033[1;33m  CONDA environment checkm-genome/checkm-genome_v1.2.0 already present. Skipping.. \033[0m'
fi

# EukRep
if [[ ! -d "$conda_location/eukrep/eukrep_v0.6.7" ]]
then
    echo -e '\033[1;33m eukrep/eukrep_v0.6.7\033[0m'
    mkdir -p $conda_location/eukrep/
    mamba create --yes --prefix $conda_location/eukrep/eukrep_v0.6.7 -c conda-forge -c bioconda scikit-learn==0.19.2 eukrep=0.6.7
  else
    echo -e '\033[1;33m  CONDA environment eukrep/eukrep_v0.6.7 already present. Skipping.. \033[0m'
fi

################################################################
# Pip environments
################################################################
echo -e '\n\033[1;33m Setting up Python venv..\033[0m'

# Python general UNLOCK virtual environment
python3 -m venv $python_venv_location/unlock
# Activate venv
source $python_venv_location/unlock/bin/activate
# Upgrade because we can
$python_venv_location/unlock/bin/python3 -m pip install --upgrade pip

## Pip dependencies
python3 -m pip install wheel biom-format pysam numpy pandas matplotlib rdflib python-irodsclient html5lib

python3 -m pip install cwltool==3.1.20211004060744
cwltool --version && python3 -m pip install html5lib

deactivate

################################################################
# R Packages
################################################################
# echo \n$'\e[1;33m'Setting up R packages..$'\e[0m'

# R -e "install.packages('docopt',dependencies=TRUE, repos='https://packagemanager.rstudio.com/all/__linux__/bionic/latest')"
# Rscript /scripts/install2.R -n 10 -s -l $R_location -r https://packagemanager.rstudio.com/all/__linux__/bionic/latest data.table futile.logger ggplot2 optparse plyr readr reshape2 scales viridis yaml parallel castor

################################################################
# Databases and references
################################################################
echo -e '\n\033[1;33m  Downloading references and databases..\033[0m'

ref_locations=( \
  'databases/CheckM' \ 
  'genomes/GCA_000/GCA_000001/GCA_000001405.28'
)
for ref in ${ref_locations[@]}; do\
   echo -e '\033[1;33m  '$ref '\033[0m'
  lftp --user anonymous --password anonymous -e "mirror -c -e --parallel=8 --verbose $ref /unlock/references/$ref  ;quit;" https://unlock-icat.irods.surfsara.nl:/references/
done
echo ''