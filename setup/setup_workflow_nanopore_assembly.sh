
#!/bin/bash
#==========================================================================================
#title          :Nanopore assembly CWL workflow dependencies
#description    :Nanopore assembly CWL workflow setup all dependencies
#author         :Bart Nijsse & Jasper Koehorst
#date           :2022
#version        :0.1.0
#==========================================================================================

# This script is expected to be run from the UNLOCK cwl docker container:
# https://git.wur.nl/unlock/docker/-/tree/master/cwl
# docker run -v <your dep dir>:/unlock docker-registry.wur.nl/unlock/docker:cwl /bin/bash -c "/cwl/setup_dependencies.sh"
# detailed instructions here: https://m-unlock.gitlab.io/docs/setup/setup.html

echo -e '\033[1;30m\033[47m                                                                      \033[0m';
echo -e '\033[1;30m\033[47m                      Workflow Nanopore assembly                      \033[0m';
echo -e '\033[1;30m\033[47m                                                                      \033[0m';

conda_location=/unlock/infrastructure/conda

echo -e '\n\033[1;33m First setting up other dependend workflows.. \033[0m\n'
sh /unlock/infrastructure/setup/setup_workflow_nanopore_quality.sh
sh /unlock/infrastructure/setup/setup_workflow_illumina_quality.sh
#sh /unlock/infrastructure/setup/setup_workflow_metagenomic_binning.sh

################################################################
# Tool binaries and scripts
################################################################
echo -e '\n\033[1;33m Setting up tool binaries for nanopore assembly workflow.. \033[0m\n'

tool_locations=( \
"Flye/Flye_v2.9" \
"Pilon/Pilon_v1.24" \
)
for tool in ${tool_locations[@]}; do\
  echo -e '\033[1;33m  '$tool '\033[0m'
  lftp --user anonymous --password anonymous -e "mirror -c -e --parallel=8 --verbose binaries/$tool /unlock/infrastructure/binaries/$tool ;quit;" https://unlock-icat.irods.surfsara.nl:/infrastructure/binaries
done

# Tool permissions, because webdav does not retain permissions. 
echo -e '\n\033[1;33m Setting up executing rights.. \033[0m\n'
chmod +x /unlock/infrastructure/binaries/Flye/Flye_v2.9/bin/*

################################################################
# Conda environments
################################################################
echo -e '\n\033[1;33m Setting up CONDA environments for nanopore assembly workflow.. \033[0m\n'

# Update conda
source /root/miniconda/bin/activate
# !! THIS UPDATE RUN CURRENTLY BREAKS MAMBA !!
#conda update -y -n base -c defaults conda

conda_environments=( \
"medaka/medaka_v1.6.1" \
)

for conda_env in ${conda_environments[@]}; do\
  tool=`echo $conda_env | awk -F"/" '{print $1}'`
  version=`echo $conda_env | awk -F"_" '{print $NF}' | sed 's/v//g'`
  
  if [[ ! -d "nb$conda_location/$conda_env" ]]
  then
    echo $'\e[1;33m'$conda_env$'\e[0m\n'
    mkdir -p $conda_location/$tool
    mamba create --yes --prefix $conda_location/$conda_env -c bioconda -c conda-forge $tool=$version
  else
    echo -e '\033[1;33m  CONDA environment '$conda_env' already present. Skipping.. \033[0m'
  fi
done