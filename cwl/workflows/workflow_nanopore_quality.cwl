#!/usr/bin/env cwl-runner
cwlVersion: v1.2
class: Workflow
requirements:
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  ScatterFeatureRequirement: {}

label: Nanopore Quality Control and Filtering
doc: |
  **Workflow for nanopore read quality control and contamination filtering.**
  - FastQC before filtering (read quality control)
  - Kraken2 taxonomic read classification
  - Minimap2 read filtering based on given references
  - FastQC after filtering (read quality control)<br><br>

  Other UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>

  **All tool CWL files and other workflows can be found here:**<br>
    Tools: https://gitlab.com/m-unlock/cwl<br>
    Workflows: https://gitlab.com/m-unlock/cwl/workflows<br>

  **How to setup and use an UNLOCK workflow:**<br>
  https://m-unlock.gitlab.io/docs/setup/setup.html<br>

outputs:
  reports_folder:
    type: Directory
    label: Filtering reports folder
    doc: Folder containing all reports of filtering and quality control
    outputSource: reports_files_to_folder/results
  filtered_reads:
    type: File
    label: Filtered nanopore reads
    doc: Filtered nanopore reads
    outputSource:
      - reference_filter_nanopore/fastq
      - merge_nanopore_fastq/output
    pickValue: first_non_null

inputs:
  identifier:
    type: string
    doc: Identifier for this dataset used in this workflow
    label: identifier used
  nanopore_reads:
    type: string[]
    doc: Nanopore sequence file locally fastq format
    label: Nanopore reads
  filter_references:
    type: string[]?
    doc: Contamination references fasta file for contamination filtering
    label: Contamination reference file
    # default: ["/unlock/references/databases/bbduk/GCA_000001405.28_GRCh38.p13_genomic.fna.gz"] # HUMAN

  keep_reference_mapped_reads:
    type: boolean
    doc: Keep with reads mapped to the given reference
    label: Keep mapped reads
    default: false

  kraken_database:
    type: string[]?
    label: Kraken2 database
    doc: Kraken2 database location, multiple databases is possible
    default: []

  threads:
    type: int?
    doc: Number of threads to use for computational processes
    label: Number of threads
    default: 2
  memory:
    type: int?
    doc: Maximum memory usage in megabytes
    label: Maximum memory in MB
    default: 4000

  step:
    type: int?
    label: CWL base step number
    doc: Step number for order of steps
    default: 1

  destination:
    type: string?
    label: Output Destination
    doc: Optional Output destination used for cwl-prov reporting.

steps:
#############################################
#### merging of FASTQ files to only one
  merge_nanopore_fastq:
    label: Merge fastq files
    # when: $(inputs.nanopore_reads.length > 1)
    run: ../bash/concatenate.cwl
    in:
      nanopore_reads: nanopore_reads
      identifier: identifier
      # infiles:
      #   source: [nanopore_fastq_reads]
      #   linkMerge: merge_flattened
      #   pickValue: all_non_null
      file_paths:
        source: nanopore_reads
        linkMerge: merge_flattened
        pickValue: all_non_null
      outname:
        valueFrom: $(inputs.identifier)_nanopore_raw.fastq.gz
    out: [output]
#############################################
#### FASTQC Before
  fastqc_nanopore_before:
    label: FastQC before
    doc: Quality assessment and report of reads before filter
    in:
      nanopore: merge_nanopore_fastq/output
      # strings paths because --provenance has a bug when copying files  :(
      # nanopore_path: nanopore_reads
      threads: threads
    run: ../fastqc/fastqc.cwl
    out: [html_files, zip_files]
#############################################
#### merging of contamination reference files
  prepare_bbmap_db:
    label: Prepare references
    doc: Prepare BBMap references to a single fasta file and unique headers
    when: $(inputs.filter_references !== null && inputs.filter_references.length !== 0)
    run: ../bbmap/prepare_fasta_db.cwl
    in:
      filter_references: filter_references
      fasta_location:
        source: filter_references
        linkMerge: merge_flattened
        pickValue: all_non_null
      output_file_name:
        valueFrom: "filter-reference_prepared.fa.gz"
    out: [fasta_db]
#############################################
#### taxonomic classification of reads with Kraken2
  nanopore_quality_kraken2:
    label: Kraken2
    doc: Taxonomic classification of FASTQ reads
    when: $(inputs.kraken_database !== null && inputs.kraken_database.length !== 0)
    run: ../kraken2/kraken2.cwl
    scatter: database
    in:
      tmp_id: identifier
      identifier:
        valueFrom: $(inputs.tmp_id)_nanopore_unfiltered
      threads: threads
      kraken_database: kraken_database
      database: kraken_database
      nanopore: merge_nanopore_fastq/output
      # strings paths because --provenance has a bug when copying files :(
      nanopore_path: nanopore_reads
    out: [sample_report]

  nanopore_quality_kraken2_krona:
    label: Krona
    doc: Visualization of Kraken2 classification with Krona
    when: $(inputs.kraken_database !== null && inputs.kraken_database.length !== 0)
    run: ../krona/krona.cwl
    scatter: kraken
    in:
      kraken_database: kraken_database
      kraken: nanopore_quality_kraken2/sample_report
    out: [krona_html]
#############################################
#### Reference filter
  reference_filter_nanopore:
    label: Reference mapping
    doc: Removal of contaminated reads using minimap2 mapping
    when: $(inputs.filter_references !== null && inputs.filter_references.length !== 0)
    run: ../minimap2/minimap2_to_fastq.cwl
    in:
      tmp_id: identifier
      identifier:
        valueFrom: $(inputs.tmp_id)_nanopore
      filter_references: filter_references
      threads: threads
      reference: prepare_bbmap_db/fasta_db
      reads: merge_nanopore_fastq/output
      output_mapped: keep_reference_mapped_reads
      preset:
        default: "map-ont"
    out: [fastq, log]
#############################################
#### FASTQC After
  fastqc_nanopore_after:
    label: FastQC after
    doc: Quality assessment and report of reads before filter
    when: $(inputs.filter_references !== null && inputs.filter_references.length !== 0)
    in:
      filter_references: filter_references
      nanopore: reference_filter_nanopore/fastq
      threads: threads
    run: ../fastqc/fastqc.cwl
    out: [html_files, zip_files]

#############################################
#### Move to folder if not part of a workflow
  reports_files_to_folder:
    label: Reports to folder
    doc: Preparation of fastp output files to a specific output folder
    in:
      files:
        source: [fastqc_nanopore_before/html_files, fastqc_nanopore_before/zip_files, fastqc_nanopore_after/html_files, fastqc_nanopore_after/zip_files, nanopore_quality_kraken2/sample_report, nanopore_quality_kraken2_krona/krona_html, reference_filter_nanopore/log]
        linkMerge: merge_flattened
        pickValue: all_non_null
      step: step
      destination:
        valueFrom: |
          ${
            var step = inputs.step;
            return step+"_Nanopore_Read_Quality";
          }
    run: ../expressions/files_to_folder.cwl
    out:
      [results]

s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-8172-8981
    s:email: mailto:jasper.koehorst@wur.nl
    s:name: Jasper Koehorst
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-9524-5964
    s:email: mailto:bart.nijsse@wur.nl
    s:name: Bart Nijsse

s:citation: https://m-unlock.nl
s:codeRepository: https://gitlab.com/m-unlock/cwl
s:dateCreated: "2020-00-00"
s:dateModified: "2022-05-00"
s:license: https://spdx.org/licenses/Apache-2.0
s:copyrightHolder: "UNLOCK - Unlocking Microbial Potential"


$namespaces:
  s: https://schema.org/