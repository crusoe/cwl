#!/usr/bin/env cwl-runner
cwlVersion: v1.2
class: Workflow
requirements:
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}

label: (Hybrid) Metagenomics workflow
doc: |
  **Workflow (hybrid) metagenomic assembly and binning  **<br>
    - Workflow Illumina Quality: https://workflowhub.eu/workflows/336?version=1	
      - FastQC (control)
      - fastp (quality trimming)
      - kraken2 (taxonomy)
      - bbmap contamination filter
    - Kraken2 taxonomic classification of FASTQ reads
    - SPAdes (Assembly)
    - QUAST (Assembly quality report)
    - Contig binning (OPTIONAL)
    - Workflow binnning https://workflowhub.eu/workflows/64?version=11
      - Metabat2
      - CheckM
      - BUSCO
      - GTDB-Tk
    - Workflow GEM
      - CarveMe (GEM generation)
      - MEMOTE (GEM test suite)
      - SMETANA (Species METabolic interaction ANAlysis)

  Other UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>
  
  **All tool CWL files and other workflows can be found here:**<br>
    Tools: https://gitlab.com/m-unlock/cwl<br>
    Workflows: https://gitlab.com/m-unlock/cwl/workflows<br>

  **How to setup and use an UNLOCK workflow:**<br>
  https://m-unlock.gitlab.io/docs/setup/setup.html<br>

outputs:
  read_filtering_output_keep:
    label: Read filtering output
    doc: Read filtering stats + filtered reads
    type: Directory?
    outputSource: keep_readfilter_files_to_folder/results
  read_filtering_output:
    label: Read filtering output
    doc: Read filtering stats + filtered reads
    type: Directory?
    outputSource: readfilter_files_to_folder/results

  kraken2_output:
    label: Kraken2 reports
    doc: Kraken2 taxonomic classification reports
    type: Directory
    outputSource: kraken2_files_to_folder/results
  assembly_output:
    label: Assembly output
    doc: Output from different assembly steps
    type: Directory
    outputSource: assembly_files_to_folder/results
  # bam_output:
  #   label: BAM files
  #   doc: Mapping results in indexed BAM format
  #   type: Directory?
  #   outputSource: sorted_bam_files_to_folder/results
  binning_output:
    label: Binning output
    doc: Binning outputfolders
    type: Directory?
    outputSource: binning_files_to_folder/results
  gem_output:
    label: Community GEM output
    doc: Community GEM output folder
    type: Directory?
    outputSource: GEM_files_to_folder/results

inputs:
  identifier:
    type: string
    doc: Identifier for this dataset used in this workflow
    label: Identifier used
  illumina_forward_reads:
    type: string[]
    doc: Forward sequence file path
    label: Forward reads
  illumina_reverse_reads:
    type: string[]
    doc: Reverse sequence file path
    label: Reverse reads
  pacbio_reads:
    type: string[]?
    doc: File with PacBio reads locally
    label: PacBio reads
  nanopore_reads:
    type: string[]?
    doc: File with PacBio reads locally
    label: PacBio reads
  filter_references:
    type: string[]?
    doc: bbmap reference fasta file paths for contamination filtering
    label: Contamination reference file
  use_reference_mapped_reads:
    type: boolean
    doc: Continue with reads mapped to the given reference
    label: Keep mapped reads
    default: false
  keep_filtered_reads:
    type: boolean
    doc: Keep filtered reads in the final output
    label: Keep filtered reads
    default: false
  deduplicate:
    type: boolean?
    doc: Remove exact duplicate reads with fastp
    label: Deduplicate reads
    default: false
  kraken_database:
    type: string[]?
    doc: Absolute path with database location of kraken2
    label: Kraken2 database
    default: []

  ont_basecall_model:
    type: string?
    label: ONT Basecalling model
    doc: |
      Basecalling model used with guppy default r941_min_high. 
      Available: r941_trans, r941_flip213, r941_flip235, r941_min_fast, r941_min_high, r941_prom_fast, r941_prom_high. (required)

  pilon_fixlist:
    type: string
    label: Pilon fix list
    doc: A comma-separated list of categories of issues to try to fix
    default: "snps,gaps,local"

  metagenome:
    type: boolean?
    default: true
    doc: Metagenome option for assemblers
    label: When working with metagenomes

  run_spades:
    type: boolean?
    label: Use SPAdes
    doc: Run with SPAdes assembler
    default: true
  run_flye:
    type: boolean?
    label: Use Flye
    doc: Run with Flye assembler
    default: false
  run_pilon:
    type: boolean?
    label: Use Pilon
    doc: Run with Pilon illumina assembly polishing
    default: false

  binning:
    type: boolean?
    label: Run binning workflow
    doc: Run with contig binning workflow
    default: false
  run_GEM:
    type: boolean?
    label: Run GEM workflow
    doc: Run the community genomescale metabolic models workflow on bins
    default: false
    
  # There are some issues with the --tmpdir-prefix option in cwltool and the gtdbtk tool
  run_gtdbtk:
    type: boolean
    label: Run GTDB-Tk
    doc: Run GTDB-Tk taxonomic bin classification when true
    default: true
  run_smetana:
    type: boolean?
    label: Run SMETANA
    doc: Run SMETANA (Species METabolic interaction ANAlysis)
    default: false

  threads:
    type: int?
    doc: Number of threads to use for computational processes
    label: Number of threads
    default: 2
  memory:
    type: int?
    doc: Maximum memory usage in megabytes
    label: Memory usage (MB)
    default: 4000
  
  destination:
    type: string?
    label: Output Destination (prov only)
    doc: Not used in this workflow. Output destination used for cwl-prov reporting only.

steps:
#############################################
#### Workflow for quality and filtering using fastqc, fastp and optionally bbduk
  workflow_quality_illumina:
    label: Quality and filtering workflow
    doc: Quality assessment of illumina reads with rRNA filtering option
    when: $(inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: workflow_illumina_quality.cwl
    in:
      forward_reads: illumina_forward_reads
      reverse_reads: illumina_reverse_reads
      filter_references: filter_references
      keep_reference_mapped_reads: use_reference_mapped_reads
      kraken_database: kraken_database
      deduplicate: deduplicate
      memory: memory
      threads: threads
      identifier: identifier
      step:
        default: 1
    out: [QC_reverse_reads, QC_forward_reads, reports_folder]

#############################################
#### Quality Nanopore
  workflow_quality_nanopore:
    label: Nanopore quality and filtering workflow
    doc: Quality and filtering workflow for nanopore reads
    when: $(inputs.nanopore_reads !== null && inputs.nanopore_reads !== 0)
    run: workflow_nanopore_quality.cwl
    in:
      nanopore_reads: nanopore_reads
      filter_references: filter_references
      keep_reference_mapped_reads: use_reference_mapped_reads
      threads: threads
      identifier: identifier
      kraken_database: kraken_database
      step: 
        default: 1
    out: [filtered_reads, reports_folder]

#############################################
#### Taxonomic classification of with Kraken2
  nanopore_kraken2:
    label: Kraken2 Nanopore
    doc: Taxonomic classification of nanopore FASTQ reads
    when: $(inputs.database !== null && inputs.database.length !== 0 && inputs.nanopore_raw_reads !== null && inputs.nanopore_reads !== 0)
    run: ../kraken2/kraken2.cwl
    scatter: database
    in:
      tmp_id: identifier
      identifier:
        valueFrom: $(inputs.tmp_id)_nanopore_filtered
      threads: threads
      database: kraken_database
      nanopore: workflow_quality_nanopore/filtered_reads
    out: [standard_report, sample_report]

  illumina_kraken2:
    label: Kraken2 Illumina
    doc: Taxonomic classification of illumina FASTQ reads
    when: $(inputs.database !== null && inputs.database.length !== 0 && inputs.illumina_raw_forward_reads !== null && inputs.illumina_raw_forward_reads.length !== 0)
    run: ../kraken2/kraken2.cwl
    scatter: database
    in:
      illumina_raw_forward_reads: illumina_forward_reads
      tmp_id: identifier
      identifier:
        valueFrom: $(inputs.tmp_id)_filtered_illumina
      threads: threads
      database: kraken_database
      forward_reads: workflow_quality_illumina/QC_forward_reads
      reverse_reads: workflow_quality_illumina/QC_reverse_reads
      paired_end:
        default: true
    out: [standard_report, sample_report]

  kraken2_compress:
    label: Compress kraken2
    doc: Compress large kraken2 report file 
    when: $(inputs.kraken_database !== null && inputs.kraken_database.length !== 0)
    run: ../bash/pigz.cwl
    scatter: inputfile
    in:
      kraken_database: kraken_database

      inputfile:
        source: [nanopore_kraken2/standard_report, illumina_kraken2/standard_report]
        linkMerge: merge_flattened
        pickValue: all_non_null
      threads: threads
    out: [outfile]

  kraken2_krona:
    label: Krona Kraken2
    doc: Visualization of kraken2 with Krona
    when: $(inputs.kraken_database !== null && inputs.kraken_database.length !== 0)
    run: ../krona/krona.cwl
    scatter: kraken
    in:
      kraken_database: kraken_database
      kraken:
        source: [nanopore_kraken2/sample_report, illumina_kraken2/sample_report]
        linkMerge: merge_flattened
        pickValue: all_non_null
    out: [krona_html]

#############################################
#### Assembly using SPADes
  spades:
    doc: Genome assembly using spades with illumina/pacbio reads
    label: SPAdes assembly
    when: $(inputs.run_spades && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: ../spades/spades.cwl
    in:
      run_spades: run_spades

      forward_reads:
        source: [ workflow_quality_illumina/QC_forward_reads ]
        linkMerge: merge_nested
      reverse_reads:
        source: [ workflow_quality_illumina/QC_reverse_reads ]
        linkMerge: merge_nested
      pacbio_reads: pacbio_reads
      nanopore_reads: nanopore_reads
      metagenomics:
        default: true
      memory: memory
      threads: threads
    out: [contigs, scaffolds, assembly_graph, contigs_assembly_paths, scaffolds_assembly_paths, contigs_before_rr, params, log, internal_config, internal_dataset]

  compress_spades:
    label: SPAdes compressed
    doc: Compress the large Spades assembly output files
    when: $(inputs.run_spades && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: ../bash/pigz.cwl
    scatter: [inputfile]
    scatterMethod: dotproduct
    in:
      run_spades: run_spades
      forward_reads: illumina_forward_reads

      threads: threads
      inputfile:
        source: [spades/contigs, spades/scaffolds,spades/assembly_graph,spades/contigs_before_rr, spades/contigs_assembly_paths,spades/scaffolds_assembly_paths]
        linkMerge: merge_flattened
        pickValue: all_non_null
    out: [outfile]

#############################################
#### De novo assembly with Flye
  flye:
    label: Nanopore Flye assembly
    doc: De novo assembly of single-molecule reads with Flye
    when: $(inputs.run_flye)
    run: ../flye/flye.cwl
    in:
      run_flye: run_flye

      nano_raw: workflow_quality_nanopore/filtered_reads
      threads: threads
      metagenome: metagenome
    out: [00_assembly, 10_consensus, 20_repeat, 30_contigger, 40_polishing, assembly, assembly_info, flye_log, params]

#############################################
#### Polishing of assembled genome with Medaka
  medaka:
    label: Medaka polishing of assembly
    doc: Medaka for polishing of assembled genome
    when: $(inputs.nanopore_reads !== null && inputs.nanopore_reads !== 0)
    run: ../medaka/medaka_py.cwl
    in:
      nanopore_reads: nanopore_reads
      threads: threads
      draft_assembly: 
        source:
        - flye/assembly
        - spades/scaffolds
        pickValue: first_non_null
      reads: workflow_quality_nanopore/filtered_reads
      basecall_model: ont_basecall_model
    out: [polished_assembly, gaps_in_draft_coords] # probs, calls_to_draft
  metaquast_medaka:
    label: assembly evaluation
    doc: evaluation of polished assembly with metaQUAST
    when: $(inputs.nanopore_reads !== null && inputs.nanopore_reads !== 0)
    run: ../quast/metaquast.cwl
    in:
      nanopore_reads: nanopore_reads
      assembly: medaka/polished_assembly
      threads: threads
    out: [metaquast_outdir, meta_combined_ref, meta_icarusDir, metaquast_krona, not_aligned, meta_downloaded_ref, runs_per_reference, meta_summary, meta_icarus, metaquast_log, metaquast_report, basicStats, quast_icarusDir, quast_icarusHtml, quastReport, quastLog, transposedReport]

#############################################
#### Workflow Pilon assembly polishing
  workflow_pilon:
    label: Pilon worklow
    doc: Illumina reads assembly polishing with Pilon
    when: $(inputs.run_pilon && inputs.illumina_forward_reads !== null && inputs.illumina_forward_reads.length !== 0)
    run: workflow_pilon_mapping.cwl
    in:
      run_pilon: run_pilon

      identifier: identifier
      assembly:
        source:
        - medaka/polished_assembly
        - spades/scaffolds
        pickValue: first_non_null
      illumina_forward_reads: workflow_quality_illumina/QC_forward_reads
      illumina_reverse_reads: workflow_quality_illumina/QC_reverse_reads
      fixlist: pilon_fixlist
      threads: threads
      memory: memory
    out: [pilon_polished_assembly, vcf, log]

#############################################
# #### Assembly quality assessment using quast
#   quast:
#     doc: Genome assembly quality assessment using Quast
#     label: Quast workflow
#     run: ../quast/quast.cwl
#     in:
#       assembly: workflow_pilon/pilon_polished_assembly
#     out: [basicStats, icarusDir, icarusHtml, quastReport, quastLog, transposedReport]
#############################################
#### Assembly evaluation with QUAST
  metaquast_pilon:
    label: Illumina assembly evaluation
    doc: Illumina evaluation of pilon polished assembly with metaQUAST
    when: $(inputs.run_pilon && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: ../quast/metaquast.cwl
    in:
      run_pilon: run_pilon
      forward_reads: illumina_forward_reads

      assembly: workflow_pilon/pilon_polished_assembly
      threads: threads
    out: [metaquast_outdir, meta_combined_ref, meta_icarusDir, metaquast_krona, not_aligned, meta_downloaded_ref, runs_per_reference, meta_summary, meta_icarus, metaquast_log, metaquast_report, basicStats, quast_icarusDir, quast_icarusHtml, quastReport, quastLog, transposedReport]

#############################################
#### Read mapping using BBmap for binning
  bbmap:
    label: BBmap read mapping
    doc: Illumina read mapping using BBmap on assembled contigs
    when: $(inputs.binning && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: ../bbmap/bbmap.cwl
    in:
      binning: binning

      identifier: identifier
      reference: 
        source:
        - workflow_pilon/pilon_polished_assembly
        - medaka/polished_assembly
        - spades/scaffolds
        pickValue: first_non_null
      forward_reads: workflow_quality_illumina/QC_forward_reads
      reverse_reads: workflow_quality_illumina/QC_reverse_reads
      memory: memory
      threads: threads
    out: [sam, stats, covstats, log]
#############################################
#### Convert sam file to sorted bam for binning
  sam_to_sorted_bam:
    label: sam conversion to sorted bam
    doc: Sam file conversion to a sorted indexed bam file
    when: $(inputs.binning && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: ../samtools/sam_to_sorted-bam.cwl
    in:
      binning: binning
      forward_reads: illumina_forward_reads

      identifier: identifier
      sam: bbmap/sam
      threads: threads
    out: [sortedbam]
#############################################
#### reports per contig alignment statistics for binning
  contig_read_counts:
    label: Samtools idxstats
    doc: Reports alignment summary statistics
    when: $(inputs.binning && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: ../samtools/samtools_idxstats.cwl
    in:
      binning: binning
      forward_reads: illumina_forward_reads

      identifier: identifier
      bam_file: sam_to_sorted_bam/sortedbam
      threads: threads
    out: [contigReadCounts]

#############################################
#### Binning workflow
  workflow_binning:
    label: Binning workflow
    doc: Binning workflow to create bins
    when: $(inputs.binning && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: workflow_metagenomics_binning.cwl
    in:
      binning: binning
      forward_reads: illumina_forward_reads

      identifier: identifier
      assembly:
        source:
        - workflow_pilon/pilon_polished_assembly
        - medaka/polished_assembly
        - spades/scaffolds
        pickValue: first_non_null
      bam_file: sam_to_sorted_bam/sortedbam
      threads: threads
      memory: memory
      run_gtdbtk: run_gtdbtk
      step: 
        default: 1
    out: [bins,metabat2_output,checkm_output,gtdbtk_output,busco_output, bins_summary_table, eukrep_fasta, eukrep_stats_file]
#############################################
#### GEM workflow
  workflow_GEM:
    label: GEM workflow
    doc: CarveMe community genomescale metabolic models workflow from bins
    when: $(inputs.binning && inputs.run_GEM)
    run: workflow_metagenomics_GEM.cwl
    in:
      binning: binning
      run_smetana: run_smetana
      run_GEM: run_GEM
      identifier: identifier
      bins: workflow_binning/bins
      threads: threads
    out: [carveme_gems_folder,protein_fasta_folder,memote_folder,smetana_output,gemstats_out]

#############################################

# OUTPUT FOLDER PREPARATION #

#############################################
#### Filtered reads output folder (keep)
  keep_readfilter_files_to_folder:
    doc: Preparation of read filtering output files to a specific output folder
    label: Read filtering output folder
    when: $(inputs.keep_filtered_reads)
    run: ../expressions/files_to_folder.cwl
    in:
      keep_filtered_reads: keep_filtered_reads
      
      files:
        source: [workflow_quality_nanopore/filtered_reads, workflow_quality_illumina/QC_forward_reads, workflow_quality_illumina/QC_reverse_reads]
        linkMerge: merge_flattened
      folders:
        source: [workflow_quality_nanopore/reports_folder,workflow_quality_illumina/reports_folder]
        linkMerge: merge_flattened
      destination:
        valueFrom: $("1_Read_Filtering")
    out:
      [results]
#############################################
#### Filtered reads output folder
  readfilter_files_to_folder:
    doc: Preparation of read filtering output files to a specific output folder
    label: Read filtering output folder
    when: $(inputs.keep_filtered_reads === false)
    run: ../expressions/files_to_folder.cwl
    in:
      keep_filtered_reads: keep_filtered_reads

      folders:
        source: [workflow_quality_nanopore/reports_folder,workflow_quality_illumina/reports_folder]
        linkMerge: merge_flattened
      destination:
        valueFrom: $("1_Read_Filtering")
    out:
      [results]

#############################################    
#### Kraken2 output folder
  kraken2_files_to_folder:
    doc: Preparation of Kraken2 output files to a specific output folder
    label: Kraken2 output folder
    run: ../expressions/files_to_folder.cwl
    in:
      files:
        source: [kraken2_compress/outfile, kraken2_krona/krona_html, nanopore_kraken2/sample_report, illumina_kraken2/sample_report]
        linkMerge: merge_flattened
      destination:
        valueFrom: $("2_Kraken2_classification")
    out:
      [results]
#############################################
#### SPAdes Output folder
  spades_files_to_folder:
    doc: Preparation of SPAdes output files to a specific output folder
    label: SPADES output to folder
    when: $(inputs.run_spades)
    run: ../expressions/files_to_folder.cwl
    in:
      run_spades: run_spades
      files:
        source: [compress_spades/outfile, spades/params, spades/log, spades/internal_config, spades/internal_dataset]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: $("SPAdes_Assembly")
    out:
      [results]
#############################################
#### Flye output folder
  flye_files_to_folder:
    doc: Preparation of Flye output files to a specific output folder
    label: Flye output folder
    when: $(inputs.run_flye)
    run: ../expressions/files_to_folder.cwl
    in:
      run_flye: run_flye
      files:
        source: [flye/assembly, flye/assembly_info, flye/flye_log, flye/params]
        linkMerge: merge_flattened
      # folders:
        # source: [workflow_flye/00_assembly, workflow_flye/10_consensus, workflow_flye/20_repeat, workflow_flye/30_contigger, workflow_flye/40_polishing]
        # linkMerge: merge_flattened
      destination:
        valueFrom: $("Flye_Assembly")
    out:
      [results]
#############################################
#### Medaka metaQUAST output folder
  metaquast_medaka_files_to_folder:
    doc: Preparation of metaQUAST output files to a specific output folder
    label: Nanopore metaQUAST output folder
    when: $(inputs.nanopore_reads !== null && inputs.nanopore_reads !== 0)
    run: ../expressions/files_to_folder.cwl
    in:
      nanopore_reads: nanopore_reads
      files: 
        source: [metaquast_medaka/metaquast_report, metaquast_medaka/quastReport]
        linkMerge: merge_flattened
        pickValue: all_non_null
      folders:
        source: [metaquast_medaka/metaquast_krona, metaquast_medaka/not_aligned, metaquast_medaka/runs_per_reference]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: $("QUAST_Medaka_assembly_quality")
    out:
      [results]
#############################################
#### Medaka + metaquast output folder
  medaka_files_to_folder:
    doc: Preparation of Medaka output files to a specific output folder
    label: Medaka output folder
    when: $(inputs.nanopore_reads !== null && inputs.nanopore_reads !== 0)
    run: ../expressions/files_to_folder.cwl
    in:
      nanopore_reads: nanopore_reads
      files:
        source: [medaka/polished_assembly, medaka/gaps_in_draft_coords] # , workflow_medaka/probs, workflow_medaka/calls_to_draft
        linkMerge: merge_flattened
        pickValue: all_non_null
      folders:
        source: [metaquast_medaka_files_to_folder/results]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: $("Medaka_assembly_polishing")
    out:
      [results]
#############################################
#### Pilon metaQUAST output folder
  metaquast_pilon_files_to_folder:
    doc: Preparation of QUAST output files to a specific output folder
    label: Illumina metaQUAST output folder
    when: $(inputs.illumina_forward_reads !== null && inputs.illumina_forward_reads.length !== 0)
    run: ../expressions/files_to_folder.cwl
    in:
      illumina_forward_reads: illumina_forward_reads
      files: 
        source: [metaquast_pilon/metaquast_report, metaquast_pilon/quastReport]
        linkMerge: merge_flattened
        pickValue: all_non_null
      folders:
        source: [metaquast_pilon/metaquast_krona, metaquast_pilon/not_aligned]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: $("QUAST_Illumina_polished_assembly_quality")
    out:
      [results]
#############################################
#### Pilon + metaQUAST output folder
  pilon_files_to_folder:
    doc: Preparation of pilon output files to a specific output folder
    label: Pilon output folder
    when: $(inputs.illumina_forward_reads !== null && inputs.illumina_forward_reads.length !== 0)
    run: ../expressions/files_to_folder.cwl
    in:
      illumina_forward_reads: illumina_forward_reads
      files: 
        source: [workflow_pilon/vcf, workflow_pilon/pilon_polished_assembly, workflow_pilon/log]
        linkMerge: merge_flattened
        pickValue: all_non_null
      folders:
        source: [metaquast_pilon_files_to_folder/results]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: $("Illumina_polished_assembly")
    out:
      [results]


#############################################
#### Combined assembly steps output folder
  assembly_files_to_folder:
    doc: Preparation of Flye output files to a specific output folder
    label: Flye output folder
    run: ../expressions/files_to_folder.cwl
    in:
      folders:
        source: [spades_files_to_folder/results, flye_files_to_folder/results, medaka_files_to_folder/results, pilon_files_to_folder/results]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: $("3_Assembly")
    out:
      [results]

#############################################
#### BAM files output folder
  # sorted_bam_files_to_folder:
  #   doc: Preparation of mapped reads (sorted bam files) to a specific output folder
  #   label: Mapped reads output to folder
  #   when: $(inputs.binning)
  #   run: ../expressions/files_to_folder.cwl
  #   in:
  #     binning: binning
  #     files: 
  #       source: [sam_to_sorted_bam/sortedbam, bbmap/stats, contig_read_counts/contigReadCounts, bbmap/covstats, bbmap/log]
  #       linkMerge: merge_flattened
  #     destination:
  #       valueFrom: $("5_BBMAP_ReadMapping")
  #   out:
  #     [results]
#############################################
#### Binning output folder
  binning_files_to_folder:
    doc: Preparation of binning output files and folders to a specific output folder
    label: Binning output to folder
    when: $(inputs.binning)
    run: ../expressions/files_to_folder.cwl
    in:
      binning: binning
      folders:
        source: [workflow_binning/metabat2_output,workflow_binning/checkm_output, workflow_binning/gtdbtk_output, workflow_binning/busco_output]
        linkMerge: merge_flattened
        pickValue: all_non_null
      files:
        source: [workflow_binning/bins_summary_table, workflow_binning/eukrep_fasta, workflow_binning/eukrep_stats_file]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: $("4_Binning")
    out:
      [results]
#############################################
#### GEMs output folder
  GEM_files_to_folder:
    doc: Preparation of GEM workflow output files and folders to a specific output folder
    label: GEM workflow output to folder
    when: $(inputs.binning && inputs.run_GEM)
    run: ../expressions/files_to_folder.cwl
    in:
      binning: binning
      run_GEM: run_GEM
      folders:
        source: [workflow_GEM/carveme_gems_folder, workflow_GEM/protein_fasta_folder, workflow_GEM/memote_folder]
        linkMerge: merge_flattened
        pickValue: all_non_null
      files: 
        source: [workflow_GEM/smetana_output, workflow_GEM/gemstats_out]
        linkMerge: merge_flattened
      destination:
        valueFrom: $("5_metaGEM")
    out:
      [results]
#############################################

s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-8172-8981
    s:email: mailto:jasper.koehorst@wur.nl
    s:name: Jasper Koehorst
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-9524-5964
    s:email: mailto:bart.nijsse@wur.nl
    s:name: Bart Nijsse

s:citation: https://m-unlock.nl
s:codeRepository: https://gitlab.com/m-unlock/cwl
s:dateCreated: "2020-00-00"
s:dateModified: "2022-08-00"
s:license: https://spdx.org/licenses/Apache-2.0 
s:copyrightHolder: "UNLOCK - Unlocking Microbial Potential"

$namespaces:
  s: https://schema.org/